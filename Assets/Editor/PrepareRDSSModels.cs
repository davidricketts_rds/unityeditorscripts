using UnityEngine;
using UnityEditor;

public class PrepareRDSSModels : AssetPostprocessor
{
    public void OnPostprocessModel(GameObject g)
    {
        MeshFilter[] mfa = g.GetComponentsInChildren<MeshFilter>();
        foreach (MeshFilter mf in mfa)
        {
            Vector3[] vertexPoint = mf.sharedMesh.vertices;
            Quaternion rot = Quaternion.Euler(-90f,0,0);
            Matrix4x4 m = Matrix4x4.TRS(mf.transform.position, rot, Vector3.one);
            for (int i = 0; i < vertexPoint.Length; i++)
            {
                Vector3 v = new Vector3(vertexPoint[i].x, vertexPoint[i].y, vertexPoint[i].z);
                vertexPoint[i] = m.MultiplyPoint3x4(v);
            }
            mf.sharedMesh.vertices = vertexPoint;
            mf.sharedMesh.RecalculateBounds();
            mf.sharedMesh.RecalculateNormals();
            mf.transform.eulerAngles = new Vector3(0,0,0);
        }
    }
}