using UnityEngine;
using UnityEditor;

public class ExportAssetBundles
{
    [MenuItem("RDSS Tools/Build Product AssetBundles")]
    static void ExportResourceProducts ()
    {
        // Bring up save panel
        string path = EditorUtility.SaveFolderPanel ("Select save folder:", "", "");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
            for (int i = 0; i < selection.Length; i++)
            {
                string name = selection[i].ToString();
                Object[] objs = new Object[1];
                objs[0] = selection[i];
                BuildPipeline.BuildAssetBundle(selection[i], objs, (path + "/" + name + ".unity3d"),
                              BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets);
            }
        }
    }

    [MenuItem("RDSS Tools/Build NonProduct AssetBundles")]
    static void ExportResourceNonProducts ()
    {
        // Bring up save panel
        string path = EditorUtility.SaveFolderPanel ("Select root assets folder:", "", "");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
            for (int i = 0; i < selection.Length; i++)
            {
                string name = selection[i].name.ToString();
                string[] folderName = name.Split(new char[1]{'-'});
                Object[] objs = new Object[1];
                objs[0] = selection[i];
                BuildPipeline.BuildAssetBundle(selection[i], objs, (path + "/" + folderName[0] + "/" + name + ".unity3d"),
                              BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets);
            }
        }
    }
}