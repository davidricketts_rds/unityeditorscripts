using UnityEngine;
using UnityEditor;

public class ChangeShaders
{
    [MenuItem ("RDSS Tools/ChangeShadersToUnlit")]
    static void changeShadersToUnlit()
    {
        foreach (object o in Selection.objects){
            if (o.GetType() == typeof(Material)){
                Material m = (Material) o;

                if (m.shader.name == "Diffuse"){
                    Shader newUnlit = Shader.Find("Unlit/Texture");
                    m.shader = newUnlit;
                }
                else if (m.shader.name == "Transparent/Diffuse"){
                    Shader newUnlit = Shader.Find("Unlit/Transparent");
                    m.shader = newUnlit;
                }
            }
        }
    }

    [MenuItem ("RDSS Tools/ConvertTransparentToCutout")]
    static void convertTransparentToCutout()
    {
        foreach (object o in Selection.objects){
            if (o.GetType() == typeof(Material)){
                Material m = (Material) o;

                if (m.shader.name == "Unlit/Transparent"){
                    Shader newUnlit = Shader.Find("Unlit/Transparent Cutout");
                    m.shader = newUnlit;
                }
            }
        }
    }

    [MenuItem ("RDSS Tools/ConvertCutoutToTransparent")]
    static void convertCutoutToTransparent()
    {
        foreach (object o in Selection.objects){
            if (o.GetType() == typeof(Material)){
                Material m = (Material) o;

                if (m.shader.name == "Unlit/Transparent Cutout"){
                    Shader newUnlit = Shader.Find("Unlit/Transparent");
                    m.shader = newUnlit;
                }
            }
        }
    }
}